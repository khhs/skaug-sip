﻿using System;
using System.IO;
using System.Text;

namespace SkaugSIP.SIP.Register
{
    class SIP_RespondMessage
    {
        private String[] MessageDataArr;
        public String StatusLine { get; set; }
        public String Via { get; set; }
        public String From { get; set; }
        public String To { get; set; }
        public String CallID { get; set; }
        public String CSeq { get; set; }
        public String WWWAuthenticate { get; set; }
        public String Server { get; set; }
        public String ContentLength { get; set; }

        public String Nonce { get; set; }
        public String DigestRealm { get; set; }
        public String QOP { get; set; }


        public SIP_RespondMessage(byte[] sipRespondMessageData)
        {
            try
            {
                MessageDataArr = ConvertDataToString(sipRespondMessageData);
                SetupClass();
            }
            catch { var a = 0; }
        }

        private String[] ConvertDataToString(byte[] data)
        {
            MemoryStream MemStream = new MemoryStream(data, 0, data.Length);
            BinaryReader BinReader = new BinaryReader(MemStream);
            String str = Encoding.UTF8.GetString(BinReader.ReadBytes(data.Length));
            return str.Split(new string[] { "\r\n" }, StringSplitOptions.None);
        }

        private void SetupClass()
        {
            try
            {
                foreach (String str in MessageDataArr)
                {
                    String TempStr = str.ToLower();
                    if (TempStr.EndsWith("unauthorized") || TempStr.EndsWith("ok"))
                        StatusLine = str;
                    else if (TempStr.StartsWith("via:"))
                        Via = str;
                    else if (TempStr.StartsWith("from:"))
                        From = str;
                    else if (TempStr.StartsWith("to:"))
                        To = str;
                    else if (TempStr.StartsWith("call-id:"))
                        CallID = str;
                    else if (TempStr.StartsWith("cseq:"))
                        CSeq = str;
                    else if (TempStr.StartsWith("www-authenticate:"))
                        WWWAuthenticate = str;
                    else if (TempStr.StartsWith("server:"))
                        Server = str;
                    else if (TempStr.StartsWith("content-length:"))
                        ContentLength = str;
                }

                String[] WWAuthArr = WWWAuthenticate.Split(new String[] { ": ", ", " }, StringSplitOptions.None);

                DigestRealm = WWAuthArr[1].Split(new String[] { "\"" }, StringSplitOptions.None)[1];
                Nonce = WWAuthArr[2].Split(new String[] { "\"" }, StringSplitOptions.None)[1];
                QOP = WWAuthArr[3].Split(new String[] { "\"" }, StringSplitOptions.None)[1];
            }
            catch
            {
                
            }
        }
    }
}
