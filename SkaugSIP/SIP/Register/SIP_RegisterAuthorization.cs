﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SkaugSIP.SIP.Register
{
    class SIP_RegisterAuthorization
    {
        public SIP_Contact SipContact { get; set; }
        public String DigestRealm { get; set; }
        public String Nonce { get; set; }
        public String Response { get; set; }
        public String CNonce { get; set; }
        public String NonceCounter = "00000001";
        public String QOP { get; set; }
        

        public SIP_RegisterAuthorization(SIP_Contact sipContact, SIP_RespondMessage sipRespondMessage)
        {
            SipContact = sipContact;

            DigestRealm = sipRespondMessage.DigestRealm;
            Nonce = sipRespondMessage.Nonce;
            QOP = sipRespondMessage.QOP;

            GetEncryptedPassword();
        }

        public override String ToString()
        {
            return string.Format("Authorization: Digest username=\"{0}\",realm=\"{1}\",nonce=\"{4}\",uri=\"sip:{3}\",response=\"{7}\",cnonce=\"{6}\",nc={5},qop={2},algorithm=MD5",
            SipContact.Username, DigestRealm, QOP, SipContact.Domain, Nonce, NonceCounter, CNonce, Response);
        }

        private void GetEncryptedPassword()
        {
            var HA1 = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(SipContact.Username + ":" + DigestRealm + ":" + SipContact.Password));
            String StrHA1 = "";
            foreach (byte b in HA1)
            {
                StrHA1 += b.ToString("x2");
            }

            var HA2 = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes("REGISTER:sip:" + SipContact.Domain));
            String StrHA2 = "";
            foreach (byte b in HA2)
            {
                StrHA2 += b.ToString("x2");
            }

            Random rand = new Random();
            CNonce = rand.Next(10000, 99999) + "";
            CNonce += rand.Next(10000, 99999) + "";
            var ResponseByteArr = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(StrHA1 + ":" + Nonce + ":"+NonceCounter+":" + CNonce + ":" + QOP + ":" + StrHA2));
            Response = "";
            foreach (byte b in ResponseByteArr)
            {
                Response += b.ToString("x2");
            }
        }

    }
}