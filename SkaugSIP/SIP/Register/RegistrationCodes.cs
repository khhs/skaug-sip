﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkaugSIP.SIP.Register
{
    static class RegistrationCodes
    {
        public static int Registering = 0;
        public static int Registered = 1;
        public static int RegisteringAuthorization = 2;
        public static int RegistrationFailed = 3;
        public static int Unregistered = 4;
    }
}
