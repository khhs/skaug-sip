﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SkaugSIP.SIP.Register
{
    class SIP_RegisterMessage
    {

        String _callid;
        String _contact;
        int _contentlength = 0;
        int _cseq = 1;
        int _expires = 3600;
        String _from;
        String _method;
        String _requestline;
        String _to;
        String _via;
        
        public String CallID { get { return "Call-ID: " + _callid; } set { _callid = value; } }
        public String Contact { get { return "Contact: " + _contact; } set { _contact = value; } }
        public String ContentLength { get { return "Content-Length: " + _contentlength; } set { int.TryParse(value, out _contentlength); } }
        public String CSeq { get { return "CSeq: " + _cseq + " " + Method; } set { int.TryParse(value, out _cseq); } }
        public String Expires { get { return "Expires: " + _expires; } set { int.TryParse(value, out _expires); } }
        public String From { get { return "From: " + _from; } set { _from = value; } }
        public String MaxForwards { get { return "Max-Forwards: 70"; } }
        public String Method { get { return _method; } set { _method = value; } }
        public String RequestLine { get { return _requestline + " SIP/2.0"; } set { _requestline = value; } }
        public String To { get { return "To: " + _to; } set { _to = value; } }
        public String UserAgent { get { return "User-Agent: SIP SDK"; } }
        public String Via { get { return "Via: SIP/2.0/UDP " + _via; } set { _via = value; } }

        public SIP_RegisterMessage()
        {

        }

        public SIP_RegisterMessage(SIP_Contact sipContact)
        {
            Method = "REGISTER";
            Via = ((IPEndPoint)sipContact.ClientSocket.LocalEndPoint).Address + ":" + ((IPEndPoint)sipContact.ClientSocket.LocalEndPoint).Port;
            From = '"' + sipContact.Username + '"' + " <" + "sip:" + sipContact.ToString() + ">";
            To = '"' + sipContact.Username + '"' + " <" + "sip:" + sipContact.ToString() + ">";
            CallID = RandomGenerator.Generate() + "-" + RandomGenerator.Generate() + "-" + RandomGenerator.Generate();
            Contact = "sip:" + sipContact.Username + "@" + ((IPEndPoint)sipContact.ClientSocket.LocalEndPoint).Address + ":" + ((IPEndPoint)sipContact.ClientSocket.LocalEndPoint).Port;
            RequestLine = Method + " sip:" + sipContact.Domain;
            Expires = sipContact.Expires+"";
        }
        
        public Byte[] GetMessageAsByteArray()
        {
            return Encoding.UTF8.GetBytes(RequestLine + "\r\n" + Via + "\r\n" + MaxForwards + "\r\n" + From + "\r\n" + To + "\r\n" + CallID + "\r\n" + CSeq + "\r\n" +
                Contact + "\r\n" + ContentLength + "\r\n" + Expires + "\r\n" + UserAgent + "\r\n\n");
        }

    }
}
