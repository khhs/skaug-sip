﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace SkaugSIP.SIP.Register
{
    class Registerer
    {
        public event EventHandler<int> OnRegistering;
        
        SIP_Contact SipContact;
        IPEndPoint ServerIp;
        byte[] ByteData = new byte[1500];

        SIP_RegisterMessageCreater RegistrationMessageCreater;

        public Registerer()
        {
        }

        public void Register(SIP_Contact sipContact)
        {
            SipContact = sipContact;
            ServerIp = new IPEndPoint(Dns.GetHostEntry(SipContact.Domain).AddressList[0], 5060);
            SipContact.ClientSocket.Connect(ServerIp);
            RegistrationMessageCreater = new SIP_RegisterMessageCreater(SipContact);

            SendRegistrationMessage();
        }

        private void SendRegistrationMessage()
        {
            SipContact.ClientSocket.BeginReceive(ByteData, 0, ByteData.Length, SocketFlags.None, new AsyncCallback(OnRecieve), null);

            Byte[] MessageData = RegistrationMessageCreater.CreateRegisterMessageByteArray();

            OnRegistering(this, RegistrationCodes.Unregistered);
            SipContact.ClientSocket.SendTo(MessageData, 0, MessageData.Length, SocketFlags.None, ServerIp);
        }


        private void OnRecieve(IAsyncResult ar)
        {
            int BytesRecieved = SipContact.ClientSocket.EndReceive(ar);

            byte[] bd = new byte[BytesRecieved];

            Array.Copy(ByteData, 0, bd, 0, BytesRecieved);

            SIP_RespondMessage RespondMessage = new SIP_RespondMessage(bd);

            if (RespondMessage.CSeq.Replace(" ","").ToLower().StartsWith("cseq:1"))
            {
                OnRegistering(this, RegistrationCodes.RegisteringAuthorization);
                SendRegistrationInformation(RegistrationMessageCreater.CreateRegisterMessageByteArray(RespondMessage));
            }
            else if (RespondMessage.StatusLine.ToLower().EndsWith("ok"))
            {
                OnRegistering(this, RegistrationCodes.Registered);
            }
            else
            {
                OnRegistering(this, RegistrationCodes.RegistrationFailed);
            }

            ByteData = new byte[1500];
            SipContact.ClientSocket.BeginReceive(ByteData, 0, ByteData.Length, SocketFlags.None, new AsyncCallback(OnRecieve), null);
        }

        private void SendRegistrationInformation(Byte[] respondRequestMessage)
        {
            SipContact.ClientSocket.SendTo(respondRequestMessage, 0, respondRequestMessage.Length, SocketFlags.None, ServerIp);
        }
    }
}
