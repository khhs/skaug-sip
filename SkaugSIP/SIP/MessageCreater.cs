﻿using SkaugSIP.SIP.Register;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SkaugSIP.SIP
{
    class MessageCreater
    {

        Random Randifier;
        SIP_Contact SipContact;
        SIP_RegisterAuthorization SipRegisterAuthorizer;
        
        int _contentlength = 0;
        int _expires = 3600;
        int _cseq = 1;
        String _method;
        String _via;
        String _from;
        String _to;
        String _callid;
        String _contact;
        String _requestline;

        public String Method { get { return _method; } set { _method = value; } }
        public String Allow { get { return "Allow: SUBSCRIBE, NOTIFY, INVITE, ACK, CANCEL, BYE, REFER, INFO, OPTIONS, MESSAGE"; } }
        public String UserAgent { get { return "User-Agent: SkaugSIP SDK"; } }
        public String MaxForwards { get { return "Max-Forwards: 70"; } }
        public String CSeq { get { return "CSeq: " + _cseq + " " + Method; } set { int.TryParse(value, out _cseq); } }
        public String Expires { get { return "Expires: " + _expires; } set { int.TryParse(value, out _expires); } }
        public String ContentLength { get { return "Content-Length: " + _contentlength; } set { int.TryParse(value, out _contentlength); } }
        public String RequestLine { get { return _requestline + " SIP/2.0"; } set { _requestline = value; } }
        public String Via { get { return "Via: SIP/2.0/UDP " + _via; } set { _via = value; } }
        public String From { get { return "From: " + _from; } set { _from = value; } }
        public String To { get { return "To: " + _to; } set { _to = value; } }
        public String CallID { get { return "Call-ID: " + _callid; } set { _callid = value; } }
        public String Contact { get { return "Contact: " + _contact; } set { _contact = value; } }
        private String Authorization { get; set; }

        public MessageCreater(SIP_Contact sipContact)
        {
            Randifier = new Random();
            SipContact = sipContact;
            SetupClass();
        }


        private void SetupClass()
        {
            Via = ((IPEndPoint)SipContact.ClientSocket.LocalEndPoint).Address + ":" + ((IPEndPoint)SipContact.ClientSocket.LocalEndPoint).Port;
            From = '"' + SipContact.Username + '"' + " <" + "sip:" + SipContact.ToString() + ">";
            To = '"' + SipContact.Username + '"' + " <" + "sip:" + SipContact.ToString() + ">";
            CallID = RandomNumberGenerator() + "-" + RandomNumberGenerator() + "-" + RandomNumberGenerator();
            Contact = "sip:" + SipContact.Username + "@" + ((IPEndPoint)SipContact.ClientSocket.LocalEndPoint).Address + ":" + ((IPEndPoint)SipContact.ClientSocket.LocalEndPoint).Port;
        }


        private String CreateFirstRegisterMessage()
        {
            Method = "REGISTER";
            RequestLine = Method + " sip:" + SipContact.Domain;

            return RequestLine + "\r\n" + Via + "\r\n" + MaxForwards + "\r\n" + From + "\r\n" + To + "\r\n" + CallID + "\r\n" + CSeq + "\r\n" +
                Contact + "\r\n" + ContentLength + "\r\n" + Expires + "\r\n" + UserAgent;
        }

        private String CreateSecondRegisterMessage(SIP_RespondMessage sipRespondMessage)
        {
            SipRegisterAuthorizer = new SIP_RegisterAuthorization(SipContact, sipRespondMessage);
            Method = "REGISTER";
            RequestLine = Method + " sip:" + SipContact.Domain;
            CSeq = "2";

            return RequestLine + "\r\n" + Via + "\r\n" + MaxForwards + "\r\n" + From + "\r\n" + To + "\r\n" + CallID + "\r\n" + CSeq + "\r\n" +
                Contact + "\r\n" + ContentLength + "\r\n" + Expires + "\r\n" + UserAgent + "\r\n" + SipRegisterAuthorizer.ToString();
        }









        public Byte[] CreateRegisterMessageByteArray()
        {
            return Encoding.UTF8.GetBytes(CreateFirstRegisterMessage() + "\r\n\n");
        }
        public Byte[] CreateRegisterMessageByteArray(SIP_RespondMessage sipRespondMessage)
        {
            return Encoding.UTF8.GetBytes(CreateSecondRegisterMessage(sipRespondMessage) + "\r\n\n");
        }

        private long RandomNumberGenerator()
        {
            int GeneratedNumber = 0;
            int GeneratedNumber2 = 0;
            GeneratedNumber = Randifier.Next(10000, 99999);
            GeneratedNumber2 = Randifier.Next(10000, 99999);
            return long.Parse(GeneratedNumber + "" + GeneratedNumber2);
        }

    }
}
