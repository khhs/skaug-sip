﻿using System;
using System.Net.Sockets;

namespace SkaugSIP.SIP
{
    class SIP_Contact
    {
        public String Username { get; set; }
        public String Password { get; set; }
        public String Domain { get; set; }
        public Socket ClientSocket { get; set; }
        public int Expires { get; set; }

        public SIP_Contact(String username, String password, String domain, Socket clientSocket, int expires)
        {
            Username = username;
            Password = password;
            Domain = domain;
            ClientSocket = clientSocket;
            Expires = expires;
        }

        public override String ToString()
        {
            return String.Format("{0}@{1}", Username, Domain);
        }
    }
}
