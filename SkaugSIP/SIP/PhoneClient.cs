﻿using SkaugSIP.SIP.Invite;
using SkaugSIP.SIP.Register;
using System;
using System.Net.Sockets;

namespace SkaugSIP.SIP
{

    public class PhoneClient
    {
        public event EventHandler<String> OnRegistered;
        SIP_Contact SipContact;
        Socket ClientSocket;
        Registerer PhoneRegisterer;

        public PhoneClient(String domain, String username, String password)
        {
            ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            SipContact = new SIP_Contact(username, password, domain, ClientSocket, 3600);

            PhoneRegisterer = new Registerer();
            PhoneRegisterer.OnRegistering += PhoneRegisterer_OnRegistering;
        }

        public void Register()
        {
            PhoneRegisterer.Register(SipContact);
        }

        public void UnRegister()
        {
            SipContact.Expires = 0;
            PhoneRegisterer.Register(SipContact);
        }

        public void Call(String number)
        {
            Inviter PhoneInviter = new Inviter();
            PhoneInviter.Call(number);
        }

        private void PhoneRegisterer_OnRegistering(object sender, int e)
        {
            if (OnRegistered != null)
            {
                if(e.Equals(RegistrationCodes.Registering))
                    OnRegistered(this, "Registering");

                else if(e.Equals(RegistrationCodes.Registered))
                    OnRegistered(this, "Registered");

                else if (e.Equals(RegistrationCodes.RegisteringAuthorization))
                    OnRegistered(this, "Authorizing user");

                else if (e.Equals(RegistrationCodes.RegistrationFailed))
                    OnRegistered(this, "Registration failed");

                else if (e.Equals(RegistrationCodes.Unregistered))
                    OnRegistered(this, "Unregistered");

            }
        }
    }
}
