﻿using SkaugSIP.SIP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TestSkaugSDK
{
    class Program
    {
        static PhoneClient SipClient;

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_DomainUnload;

            SipClient = new PhoneClient("", "", "");
            SipClient.OnRegistered += SipClient_OnRegistered;

            SipClient.Register();

            Console.ReadLine();
        }

        private static void CurrentDomain_DomainUnload(object sender, EventArgs e)
        {
            SipClient.UnRegister();
        }

        private static void SipClient_OnRegistered(object source, string arg)
        {
            Console.WriteLine(arg);
        }
    }
}
